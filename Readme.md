# **E-Learning xAPI** 
Javascript e-learning based API to simplify communication to an LRS and to help track interactions and learning activities at a granular level.

## **Contents**
- [ELearningXAPI](#markdown-header-ELearningXAPIjs)
- [Dependencies](#markdown-header-dependencies)
- [Including The Library in Your Application](#markdown-header-including-the-library-in-your-application)
- [Launching Configuration](#markdown-header-launching-configuration)
- [Instantiating the library](#markdown-header-instantiating-the-library)
- [Utility Functions](#markdown-header-utility-functions)
    - [Setting an Activity](#markdown-header-setting-an-activity)
    - [Setting a Group](#markdown-header-setting-a-group)
    - [Adding Extensions](#markdown-header-adding-extensions)
    - [Creating a Parent Object](#markdown-header-creating-a-parent-object)
    - [Retrieving Lesson and Course Registration](#markdown-header-retrieving-lesson-and-course-registration)
- [Initializing a Course](#markdown-header-initializing-a-course)   
- [Terminating a Course](#markdown-header-terminating-a-course)
- [Initializing a Lesson Attempt](#markdown-header-initializing-a-lesson-attempt)
- [Suspending an Attempt](#markdown-header-suspending-an-attempt)
- [Resuming a Lesson](#markdown-header-resuming-a-lesson)
- [Completing a Lesson](#markdown-header-completing-a-lesson)
- [Scoring a Lesson](#markdown-header-scoring-a-lesson)
- [Passing and Failing a Lesson](#markdown-header-passing-and-failing-a-lesson)
- [Terminating a Lesson](#markdown-header-terminating-a-lesson)
- [Responding to an Assessment Question](#markdown-header-responding-to-an-assessment-question)
- [Viewing a Page](#markdown-header-viewing-a-page)
- [Setting a Bookmark](#markdown-header-setting-a-bookmark)
- [Getting a Bookmark](#markdown-header-getting-a-bookmark)  
- [Basic Usage](#markdown-header-basic-usage)
***
## **ELearningXAPI**
This library can be included in web based or standalone application to simplify the process of connecting and communicating to an LRS.The E-Learning xAPI Library wraps the [xapiwrapper](https://github.com/adlnet/xAPIWrapper) and uses an instance of the xapiwrapper to help build and send xAPI statements to an LRS.

## **Dependencies**
In order to start using the library install git. Git is a version control system used to track changes in source code during development.

- Download and install Git here https://git-scm.com/   

Use git to clone the E-Learning XAPI library. Open up an terminal window and run the below command. 
```
git clone https://bitbucket.org/veracitytc/veracity-xapi-libraries.git
```
## **Including the Library in Your Application**
After cloning this repository, inside the dist directory you'll find the veracity.umd.production.min.js file.

Include the library to your application by including it in a script tag.

```
<script type="text/javascript" src="veracity.umd.production.min.js"></script>
```
**OR**

Use npm to install the library. This type of installation is recommended when using frameworks like React, Vue, or Angular. 

Place the provided dist folder into the root directory where the package.json resides and use the following command to install the library.
```
npm install dist

or

yarn add file:./dist
```
Once you have installed the library via npm or yarn, use "import" to import the library.
```
import { ELearningXAPI } from "navy";
```
## **Launching Configuration**
When launching content from the LMS, the LMS will be configured to perform launch using “TinCan Launch.”  In this case, the content does not need to form the xAPI Actor or hardcode the LRS credentials. This information is provided to the content as part of launch.  The content SHALL use the information provided via launch over any locally configured information. 

For xAPI content that is web-based and that requires testing with an LRS, you’ll need to create your own credentials using the configuration object below. This configuration object will be passed to the constructor of the ELearningXAPI object. See the "Instantiating the library" section below. 


```
import base64 from 'base-64'

// create credentials configuration object
// auth format : "Basic " + encode(username+":"+password)
const config = {
  "endpoint": "https://lrs.adlnet.gov/xapi/",
  "auth": "Basic " + base64.encode("sicukj:rewriu"), 
  "actor": {
    "name": "Valerie Verifierwicz",
    "account": { homePage: "https://edipi.navy.mil", name: "1625378415" },
    "objectType": "Agent"
  },
  "platform": "Moodle 3.8.3"
};
```
Properties of the configuration object.

| Name | Type | Description |
| ------------| :-----------: | :----- |
| endpoint | string | The Endpoint URL to the LRS’s xAPI (e.g. https://lrs.navy.mil/xAPI) |
| auth     | string | A limited scope, one time use value of the authorization header used to access to the xAPI Endpoint (e.g. Basic dG9tOjEyMzQ=) |
| actor    | JSON Object | The JSON representation of the Actor Object is required in every xAPI Statement sent by LRP. In this document, the information returned from the LRS is referenced in this document as the learner Agent Object. (e.g. ```{"name":"Valerie Verifierwicz", "objectType":"Agent","account":{"homePage":"https://edipi.navy.mil","name":"0123456789"}```) |

***
## **Use**
The ELearningXAPI library provides developers easy to use functions to help you speed up development. 
Instead of manually creating each xAPI statement the ELearningXAPI library provides functions for most
use cases within interacting with a course. This library is to be used with a Course and sub lessons. 
## **Instantiating the Library**

### **ELearningXAPI(config)**
To use the library, instantiate a ELearningXAPI object like the example below.
#### **Arguments**
config (Object) : A configuration object based on the configuration credentials provided by a NETC LRS Administrator. A config object is optional and mostly used for development, but can be used in various NETC environments (e.g., development, production, etc.). If a config object isn't passed into the ELearningXAPI library then the library will perform a launch using "TinCan Launch". 

#### **Returns**
{}: Returns new instance of the ELearningXAPI library.

**Example**

```
// Create ELearningXAPI instance. This way will use the TinCan Launch approach and query the launch parameters
// Example of a query url parameter string: index.html?endpoint=https://elearningexample.lrs.io/xapi/&auth=Basic%e0Z2ljdWtqOnJlb3JpdQ==&actor={"name":"Valerie%20Verifierwicz","objectType":%20"Agent","account":%20{"homePage":%20"https://edipi.navy.mil",%20"name":%20"162537874"}}

const eLearningLib = new ELearningXAPI();


// Create ELearningXAPI instance using a config object.   
const eLearningLib = new ELearningXAPI(config);
```
***
### **ELearningXAPI.launch()**
The launch method will attempt to use the passed in lrs configuration object or url query parameters as the credentials needed to connect to an lrs. If neither can't be found then an error will be thrown. **The launch method must be called to ensure statements are sent to an lrs.**
#### **Returns**
{}: Returns any empty object if successful else an error will be thrown.

**Example**

```
// Create ELearningXAPI instance. This way will use the TinCan Launch approach
async function myApplication() {
  const eLearningLib = new ELearningXAPI();
  await eLearningLib.launch()
    .then(result => console.log(`Launch successful ${result}`))
    .catch(error => console.warn(`An error has occurred when trying to launch ${error}`))

  // if no errors then start sending some statements!!!
}

// Create ELearningXAPI instance using a config object.  
const config = {
  "endpoint": "https://lrs.adlnet.gov/xapi/",
  "auth": "Basic " + base64.encode("sicukj:rewriu"), 
  "actor": {
    "name": "Valerie Verifierwicz",
    "account": { homePage: "https://edipi.navy.mil", name: "1625378415" },
    "objectType": "Agent"
  },
  "platform": "Moodle 3.8.3"
};

async function myApplication() {
  const eLearningLib = new ELearningXAPI(config);
  await eLearningLib.launch()
    .then(result => console.log(`Launch successful ${result}`))
    .catch(error => console.warn(`An error has occurred when trying to launch ${error}`))

  // if no errors then start sending some statements!!!
}
```
***

## **Set Up Functions**
## **Activity, Group, and Extensions**
After instantiating the ELearningXAPI library, the next steps are to set up an activity and a group, and adding extensions.
Setting an activity and a group is required in order to send statements. Setting an activity helps describe the item with which the learner is interacting. In most cases, a lesson will be the object a learner is interacting with. Setting a group helps describe a relationship to another activity. In most cases, a course will be related to a lesson activity. 

### **Setting an Activity**
An activity is any object with which the learner interacts. Typically, a lesson would be the activity added to an attempt.

### **ELearningXAPI.setActivity( id, name, description )**
Use setActivity to set an activity.

#### **Arguments**
id (String) : A unique identifier for the interaction activity according to the Activity ID requirements in the NETC Common Reference Profile.

name (String): A value that represents  the official name or title of the Activity. 

description (String): A value that represents a short description of the Activity. 

**Example**

```
// setting the lesson "Advanced Airborne Sensor Lesson"
eLearningLib.setActivity(
  "https://navy.mil/netc/xapi/activities/lessons/9e32f474-af07-11ea-b3de-0242ac130004",
  "Advanced Airborne Sensor Lesson",
  "An e-learning lesson on the Advanced Airborne Sensor."
);
```
***
## **Setting a Group**
Used to identify the course activity.

### **ELearningXAPI.setGroup( id, name, description )**
Use setGroup to set a group.

#### **Arguments**
id (String) : A unique identifier for the interaction activity according to the Activity ID requirements in the NETC Common Reference Profile.

name (String): A value that represents the official name or title of the Activity. 

description (String): A value that represents a short description of the Activity. 

**Example**

```
// setting the course "P-8A Poseidon Course"
eLearningLib.addGroup(
  "https://navy.mil/netc/xapi/activities/courses/EXAMPLE-000-AAA-000-000-A0",
  "P-8A Poseidon Course",
  "An interactive e-learning course on the Navy’s P-8A aircraft."
);
```
***
## **Adding Extensions**
When used as part of Context, the Extensions Object provides a way to optionally extend xAPI Statements by including custom properties for a specialized purpose. 

If the values can be determined, the NETC E-learning Profile requires that the launch-location, school-center, and user-agent Context Extensions be included in xAPI Statements. These NETC-specific Context Extensions are only required to be set if they are identified in advance or if they can be obtained dynamically from a network address or in a configuration file. Consult the NETC Common Reference Profile for additional guidance on the appropriate values for these NETC Context Extensions.

### **ELearningXAPI.addExtensions( key, value )**
Use addExtensions to add a single extension or an object of extensions.

OR
### **ELearningXAPI.addExtensions( extensions )**


#### **Arguments**
key (String) : An IRI that helps define a specialized purpose.

value (*): Any JSON value or data structure..

or

extensions (Object) : An object containing IRI(s) and their values.

**Example**

```
// add a single extension
eLearningLib.addExtensions(
  "https://w3id.org/xapi/netc/extensions/school-center",
  "Naval Training Service Command (NTSC)"
);

// or pass in an object of extensions
const extensions = {
  "https://w3id.org/xapi/netc/extensions/school-center":
    "Naval Training Service Command (NTSC)",
  "https://w3id.org/xapi/navy/extensions/launch-location": "Ashore",
  "https://w3id.org/xapi/netc/extensions/user-agent": navigator.userAgent,
};

eLearningLib.addExtensions(extensions);

// or use the provided constants
const { SCHOOL_CENTER, LAUNCH_LOCATION, USER_AGENT } = eLearningLib.EXTENSIONS
const extensions = {
  [SCHOOL_CENTER]: "Department of Defense (DOD)",
  [LAUNCH_LOCATION]: "Ashore",
  [USER_AGENT]: navigator.userAgent,
};

eLearningLib.addExtensions(extensions);
```

Listed below are the current library extensions available.

| Key Name | Key Value |Value Description |
|:-----|:------------|:---------|
| SCHOOL_CENTER |https://w3id.org/xapi/netc/extensions/school-center |A string value describing the NETC schoolhouse or learning center that owns the content. |
| LAUNCH_LOCATION |https://w3id.org/xapi/netc/extensions/launch-location |A string value describing whether the content was launched while ashore or afloat. |
| USER_AGENT | https://w3id.org/xapi/netc/extensions/user-agent| A string value used to determine the user’s browser and operating system. |

***
## **Creating a Parent Object**
A parent object is used to identify the activity which contains the current activity, such as the activity of the SCO that contains an assessment, interaction, or objective. Throughout this document you'll see instances where a parent object is created and used within a ELearningXAPI method. In those cases, a parent is created if an Activity has a direct relation to the Activity which is the Object of the Statement.

### **ELearningXAPI.createParent( id, name, description )**
Use createParent to build a parent object.

#### **Arguments**
id (String) : A unique identifier for the interaction activity according to the Activity ID requirements in the NETC Common Reference Profile

name (String): A value that represents the official name or title of the activity which contains the current activity.

description (String): A description of the activity which contains the current activity.

#### **Returns**
{}: Returns a Parent object. 

**Example**

```
// create a parent assessment object. This object will be the parent of a assessment questions
const parent = eLearningLib.createParent(
  "https://navy.mil/netc/xapi/activities/assessments/c66edc28-aa6b-11ea-bb37-0242ac130002"
  "Overview Assessment",
  "Assessment in the xapi101 Overview lesson"
  );
```
***

#### **Returns**
An interaction component list object.

ELearningXAPI.sendInteraction()
***
## **Retrieving Lesson and Course Registration**
Retrieving a Lesson or Course registration can be useful when wanting to store certain statement registration for later use.

### **ELearningXAPI.getLessonRegistration()**
Use getLessonRegistration to get the current lesson attempt registration.

#### **Returns**
(String): A universally unique identifier for a lesson attempt or null if none exist.

### **ELearningXAPI.getCourseRegistration()**
Use getCourseRegistration to get the current course attempt registration.

#### **Returns**
(String): A universally unique identifier for a course attempt or null if none exist.

## **Activity Type Constants**
The ELearningXAPI library provides constants for developer to use with certain ELearningXAPI method calls.
When calling either initialize or terminate an activity type string is required which the library provides through the use of constants. Currently the ELearningXAPI provides either LESSON or COURSE activity types.

```
// use constant straight from the ACTIVITY Object
eLearningLib.ACTIVITY.LESSON

or

// deconstruct from the ACTIVITY object
const { LESSSON, COURSE } = eLearingLib.ACTIVITY;
```
***
## **Initializing a Course**
A course is an aggregation or collection of one or more lesson activities. A course structure may vary and is often informed by the instructional design or by the organization of lessons or other content in an LMS. A course is also sometimes referred to in NETC instructional policies as a “module” or a “course module.” A course is the highest level of granularity for e-learning content and can include one or more lessons. An Initialized Statement for a course SHALL precede the Initialized Statement for a lesson if a lesson is part of a broader course.

An Initialized Statement is required to be communicated when an attempt on a course is started.

### **ELearningXAPI.initialize( activityType )**
Use initialize to send a course "initialized" statement to the configured LRS.

#### **Arguments**
activityType (String): A string representing what activity type you want to initialize.

#### **Returns**
{}: Returns a Promise. On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code. 

Http error codes can be found here https://github.com/adlnet/xAPI-Spec/blob/master/xAPI-Communication.md#errorcodes

**Example**

```
const { COURSE } = eLearningLib.ACTIVITY;
async function myApplication() {
  let result = await eLearningLib.initialize(COURSE)
                .then(result => console.log('course initialized!'))
                .catch(error => console.log(`An error has occurred ${error}`));
}

myApplication();

```
***
## **Terminating a Course**
A Terminated Statement is recommended to be communicated when an attempt on a course is ended.

### **ELearningXAPI.terminate( activityType )**
Use startCourseAttempt to send a course "terminated" statement to the configured LRS. 
#### **Arguments**
activityType (String): A string representing what activity type you want to initialize.

#### **Returns**
{}: Returns a Promise. On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code. 

Http error codes can be found here https://github.com/adlnet/xAPI-Spec/blob/master/xAPI-Communication.md#errorcodes

**Example**

```
const { COURSE } = eLearningLib.ACTIVITY;
async function myApplication() {
  let result = await eLearningLib.terminate(COURSE)
                .then(result => console.log('course terminated!'))
                .catch(error => console.log(`An error has occurred ${error}`));
}

myApplication();
```
***
## **Initializing a Lesson Attempt**
The lesson is the most critical part of the NETC E-learning Profile since anything that is tracked with xAPI is associated with a lesson Activity. A lesson can stand alone or it can be associated with a broader course Activity. A lesson can include an introduction, pretest, one or more sections of instructional content, and a posttest. A lesson can optionally include section activities. 

An Initialized Statement is required to be communicated when an attempt on a lesson activity is started.

### **ELearningXAPI.initialize( activityType )**
Use initialize() to send a lesson "initialized" statement to the configured LRS. 
#### **Arguments**
activityType (String): A string representing what activity type you want to initialize.
#### **Returns**
{}: Returns a Promise. On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code. 

Http error codes can be found here https://github.com/adlnet/xAPI-Spec/blob/master/xAPI-Communication.md#errorcodes

**Example**

```
async function myApplication() {
  const { LESSON } = eLearningLib.ACTIVITY;
  let result = await eLearningLib.initialize(LESSON)
                .then(result => console.log('lesson initialized!'))
                .catch(error => console.log(`An error has occurred ${error}`));
}

myApplication();

```
***
## **Suspending an Attempt**
Learners can suspend their e-learning lesson activity with the intent on coming back to it at a later time. With this in mind, NETC requires content to track the state of each learning attempt by suspending and later resuming the activity.  

### **ELearningXAPI.suspend()**
Use suspend to send a "suspended" statement to the configured LRS and save the current attempt state. If values for max, min, raw, scaled, completed, and success are set those values will also be suspended. The suspend method allows an optional parameter "activityId" in case multiple lessons within a course need to be suspended. **Hint** If a course isn't locked step then using this optional parameter might come in handy when needing to suspend multiple lessons when users are jumping around within your course.

#### **Arguments**
activityId (String): A string representing what activity the library should suspend.

#### **Returns**
{}: Returns a Promise. On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code. 

Http error codes can be found here https://github.com/adlnet/xAPI-Spec/blob/master/xAPI-Communication.md#errorcodes

**Example**

```
async function myApplication() {
  let result = await eLearningLib.suspend()
               .then(result => console.log('lesson suspended!'))
               .catch(error => console.log(`An error has occurred ${error}`));
}

myApplication();
```
***
## **Resuming a Lesson**
Typically, when an attempt on a learning activity is suspended, it should be able to be resumed by the user.

### **ELearningXAPI.resume()**
Use resume to send a "resumed" statement to the configured LRS and to restore the last suspended attempt state. The resume method allows an optional parameter "activityId" in case a lesson within a course need to be resumed. If no "activityId" is passed then the last successful suspended attempt will be fetched. If values for max, min, raw, scaled, completed, and success were set prior calling resume() then those values will be restored. **Hint** If a course isn't locked step then using this optional parameter might come in handy when resuming lessons that have been suspended. 

#### **Arguments**
activityId (String): A string representing what activity the library should suspend.

#### **Returns**
{}: Returns a Promise. On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code. 

Http error codes can be found here https://github.com/adlnet/xAPI-Spec/blob/master/xAPI-Communication.md#errorcodes

**Example**

```
async function myApplication() {
  let result = await eLearningLib.resume()
                .then(result => console.log('lesson resumed!'))
                .catch(error => console.log(`An error has occurred ${error}`));
}

myApplication();
```
***
## **Completing a Lesson**
Lesson activities can be completed by the users when the intent or objectives of the lesson have been met. 

### **ELearningXAPI.setComplete( boolean )**
Use setComplete to send a "completed" statement to the configured LRS.

#### Arguments
value (Boolean): A true or false value indicating if a lesson activity or activities have been completed. 

#### Returns
{}: Returns a Promise. On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code. 

Http error codes can be found here https://github.com/adlnet/xAPI-Spec/blob/master/xAPI-Communication.md#errorcodes

**Example**

```
async function myApplication() {
  let result = await eLearningLib.setComplete( true )
                .catch(error => console.log(`An error has occurred ${error}`));
}

myApplication();
```
***
## **Scoring a Lesson**

When learners obtain a score as part of a quiz, assessment, or other activity, the score value is reported as part of the lesson activity. The use of the Scored Statement on a lesson is for granular journaling purposes and does not necessarily represent the final score of the lesson.

### **ELearningXAPI.setScore( scoreType, scoreValue )**
Use setScore to set a score value for an activity and to send a "scored" statement to the configured LRS when the score type is "scaled" or "raw". Setting the score type to "scaled" or "raw" will also set the result.score.scaled, or result.score.max property of the statement. Setting score type "min" or "max" is used for granular journaling purposes, and will be reported on lesson termination within the result.score.min or result.score.max properties. If the scaled or raw values are set then they will be stored and used within the lesson termination statement. The ELearningXAPI library provides score constants for you to use to help speed up your development.

#### **Arguments**
scoreType (String): The score type. For instance, scaled, raw, min, or max.

scoreValue (Number): The learner's score value.

#### **Returns**
{}: Returns a Promise. On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code. 

Http error codes can be found here https://github.com/adlnet/xAPI-Spec/blob/master/xAPI-Communication.md#errorcodes

**Example**

```
async function myApplication() {
  let result = await eLearningLib.setScore( eLearningLib.SCORE.SCALED, 0.9 )
                .catch(error => console.log(`An error has occurred ${error}`));
}

myApplication();
```

### Score Constants
* SCORE constants can be used within an application to help identify what kind of score type should be used within a statement.
  
| Score Types | Description                                                                                                                                                                                                            |
| :---------- | :--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| SCALED      | The score related to the experience as modified by scaling and/or normalization. Decimal number between -1 and 1, inclusive                                                                                            |
| RAW         | The score achieved by the Actor in the experience described by the Statement. This is not modified by any scaling or normalization. Decimal number between min and max (if present, otherwise unrestricted), inclusive |
| MIN         | The lowest possible score for the experience described by the Statement. Decimal number less than max (if present)                                                                                                     |
| MAX         | The highest possible score for the experience described by the Statement. Decimal number greater than min (if present)                                                                                                 |

***
## **Passing and Failing a Lesson**

If applicable in a given lesson, learners may pass or fail the success criteria. Passing a lesson is often reserved for e-learning content that contains assessments, knowledge measurement, or grading of a learner’s performance. However, a Passed Statement could also be used for knowledge checks or practice tests without a scoring requirement.

### **ELearningXAPI.setSuccess( boolean )**
Use setSuccess to send a "passed" or "failed" statement to the configured LRS based on if the value is true or false. Using setSuccess will also store the success value which is used within the result.success property of the lesson termination statement.

#### **Arguments**
value (Boolean): True if passed or False if failed. 

#### **Returns**
{}: Returns a Promise. On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code. 

Http error codes can be found here https://github.com/adlnet/xAPI-Spec/blob/master/xAPI-Communication.md#errorcodes

**Example**
```
async function myApplication() {
  // you passed!
  let result = await eLearningLib.setSuccess(true)
      .catch( error => console.log(`An error has occurred ${error}`));
}

myApplication();
```
***
## **Terminating a Lesson**
A Terminated Statement is required to be communicated when an attempt on a lesson activity is finished. If applicable, the Terminated Statement SHALL also contain the overall results associated with the lesson, such as whether it was passed, failed, completed, or if there is a score.

### **ELearningXAPI.terminate( activityType )**
Use endLessonAttempt to send a lesson "terminated" statement to the configured LRS.

#### **Returns**
{}: Returns a Promise. On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code.

**Example**

```
const { LESSON } = eLearningLib.ACTIVITY;

async function myApplication() {
  let result = await eLearningLib.terminate(LESSON)
                .catch( error => console.log(`An error has occurred ${error}`));
}

myApplication();
```
***
## **Responding to an Assessment Question**
E-learning courses often naturally include assessments as part of the instructional design strategy for measuring learning and performance objectives. The xAPI specification provides a standard way to track assessment data with built-in object definitions for computer managed instruction (CMI) interactions. These interactions are borrowed from SCORM and are intended to provide a familiar utility for capturing interaction data from an CMI interaction activity. The questions (CMI interaction activities) are children include a parent assessment context activity. 

When sending an interaction response a couple required steps are needed. First an interaction object will need to be created and stored, so it can be passed to the sendInteraction method. An interaction object will help define what interaction activity your application wants to send a response for. Although it's not required to create a correctResponsePattern array or an InteractionComponentList object, but it's recommended to ensure a richer interaction definition. In the next two section, are guides on how to create an interaction component list and a response pattern. 

## **Create an Interaction Component**
"Depending on interactionType, Interaction Activities can take additional properties, each containing a list of interaction components. These additional properties are called "interaction component lists". The following table shows the supported interaction component list(s) for an Interaction Activity with the given interactionType.".

### **ELearningXAPI.createInteractionComponentList(id, description)**
Use createInteractionComponentList to create an interaction component list object to be used with ELearningXAPI.createInteraction().
#### **Arguments**

id (String): Identifies the interaction component within the list.

description (String): A description of the interaction component.

```
    const eLearningLib = new ELearningXAPI();

    // build an array list of interaction components
    // the return is an array which can be used with createInteraction
    const choiceComponentList = [
      { id: "option_1", description: "Sphere" },
      { id: "option_2", description: "Irregularly shaped ellipsoid" },
      { id: "option_3", description: "Flat" }
    ].map(item => eLearningLib.createInteractionComponentList(item.id, item.description));
```

### **ELearningXAPI.createResponsePattern(interactionType, response)**
Use createResponsePattern to create a formatted correctResponsePattern or a learner's response.
The below example creates correctResponsePatterns to use be used with ELearningXAPI.createInteraction().
The createResponsePattern function can also be used to create a learner's response value which can be used with ELearningXAPI.sendInteraction().
To find out how to format the response value look at the below table "Correct Response Patterns and Learner's Response Pattern".

```
  const eLearningLib = new ELearningXAPI();

  // CHOICE 
  const correctResponsePattern = eLearningLib.createResponsePattern(INTERACTION.CHOICE, ['choice1', 'choice2']);
  // returns 'choice1[,]choice2'

  const correctResponsePattern = eLearningLib.createResponsePattern(INTERACTION.MATCHING, ['source1:target2', 'source2:target1']);
  // returns 'source1[:]target2[,]source2[:]target1

  const correctResponsePattern = eLearningLib.createResponsePattern(INTERACTION.NUMERIC, ['1:4']);
  // returns '1[:]4

```
#### **Arguments**
#### Required
interactionType (String): A predefined String value for the type of interaction.

response (Array): An array of strings representing the correctResponsePattern or a learner's response. 

#### **Returns**
[]: Returns an array that holds the response pattern string.
***
### **Correct Response Patterns and Learner's Response Pattern**
The table below lists the correct response pattern formats for each interaction type. 

| Type             | Format                                                                                                                                                                                                                  | Example                                                                                                                                                                                               |
| :--------------- | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **true-false**   | An array with either `true` or `false`.                                                                                                                                                                                 | `[ true ]`                                                                                                                                                                                            |
| **choice**       | An array of item ids.                                                                                                                                                                                                   | `["answer1", "answer2"]`                                                                                                                                                                              |
| **fill-in**      | An array of string responses.                                                                                                                                                                                           | `["Space: the final frontier."]`                                                                                                                                                                      |
| **long-fill-in** | An array of string responses.                                                                                                                                                                                           | `["These are the voyages of the starship Enterprise. Its continuing mission: to explore strange new worlds, to seek out new life and new civilizations, to boldly go where no one has gone before."]` |
| **likert**       | An array of a single item id                                                                                                                                                                                            | `[ "likert_0" ]`                                                                                                                                                                                      |
| **matching**     | An array of strings where the string value represents a matching pair delimited by ":" character.            | `["target1:source2","target2:source1"]`                                                                                                               |
| **performance**  | An array of strings where the string value represents a step and it's correct value delimited by ":" character.`                                                                           | `["step1:2","step2:1"]`                                                                                                                       |
| **sequencing**   | An array ordered list of item ids.                                                                                                                                                                                      | `[1,2,3,4,5,6]`                                                                                                                                                                                       |
| **numeric**      | A range of numbers represented by a minimum and a maximum delimited by [:]. Where the range does not have a maximum or does not have a minimum, that number is omitted but the delimiter is still used. E.g. [:]4 indicates a maximum for 4 and no minimum. Where the correct response or learner's response is a single number rather than a range, the single number with no delimiter can be used.                                                                                                                               |['[:]4']
| **other**        | Any format is valid within this string as appropriate for the type of interaction.                                                                                                                                      | `[ "(35.937432,-86.868896)" ]`                                                                                                                                                                        |

### **ELearningXAPI.createInteraction( interactionType, id, name, description, correctResponsesPattern, interactionComponentList )**

Use the createInteraction method to create an interaction object. To be used with ELearningXAPI.sendInteraction.

#### **Arguments**
#### Required

interactionType (String): A predefined String value for the type of interaction.

id (String): A unique identifier for the interaction according to the Activity ID requirements in the NETC Common Reference Profile


#### Optional 

name (String): A value that represents the official name or title of the interaction activity.

description (String): A value that represents a short description of the interaction activity. 

correctResponsePattern (Array): An array of patterns representing the correct response to the interaction. See the "Correct Response Patterns" table below to see the correct formatting.

interactionComponentList (Array | Object): An array list of interaction component(s) list or an object with a source and target property which values are arrays of interaction component list for a matching interaction type.

**NOTE:** See the below table for the response formats for choices, scale, source, target, and steps.

#### **Returns**
{}: Returns an interaction object.
```
// grab interaction constants
const { TRUE_FALSE, LIKERT, MATCHING} = eLearningLib.INTERACTION;

// TRUE-FALSE INTERACTION EXAMPLE
const trueFalseInteraction = eLearningLib.createInteraction(
      TRUE_FALSE,
      "https://navy.mil/netc/xapi/activities/cmi.interactions/6fd493ee-b740-11ea-b3de-0242ac130004",      
      "Question 1",
      "Is this a True or False example?",
      ["true"]
    );


// build an array list of interaction components
// the return is an array which can be used with createInteraction
const likertComponentList = [
  { id: "option_1", description: "Sphere" },
  { id: "option_2", description: "Irregularly shaped ellipsoid" },
  { id: "option_3", description: "Flat" }
].map(item => eLearningLib.createInteractionComponentList(item.id, item.description));

// LIKERT INTERACTION EXAMPLE
const likertInteraction = eLearningLib.createInteraction(
      LIKERT,
      "https://navy.mil/netc/xapi/activities/cmi.interactions/6eb705b6-bfcc-11ea-b3de-0242ac130004",
      "Question 2",
      "What is the shape of Earth?",
      ["option_3"],
      likertComponentList
);

// MATCHING EXAMPLE
const matchingSourceComponentList = [
    { id: "source1", description: "5 + 5 =" },
    { id: "source2", description: "10 + 10 =" },
].map(item => eLearningLib.createInteractionComponentList(item.id, item.description));

const matchingTargetComponentList = [
    { id: "target1", description: "10" },
    { id: "target2", description: "20" },
].map(item => eLearningLib.createInteractionComponentList(item.id, item.description));

const matchingCorrectResponsePattern = eLearningLib.createResponsePattern(INTERACTION.MATCHING, ["source2:target1"source1:target2"]);

// Since Matching has both source and target arrays as a response pattern 
// createInteraction allows you to pass in an object with properties source and target.
// The values being the correct response pattern create above

let interaction = eLearningLib.createInteraction(
    INTERACTION.MATCHING,
    "https://navy.mil/netc/xapi/activities/cmi.interacti9d1e83e2-ce13-11eb-b8bc-0242ac130003",
    "Question 7",
    "What aircraft does the P-8 replace and what are the origins?",
    matchingCorrectResponsePattern,
    { source: matchingSourceComponentList, target: matchingTargetComponentList } // passing interactionComponentList as an object
);
await eLearningLib.sendInteraction(
    eLearningLib.createResponsePattern(INTERACTION.MATCHING, learnersAnswer),
    interaction,
    interactionParent
).catch(error => console.log(error));

```

***
### **ELearningXAPI.sendInteraction( response, interaction, parent, otherResponse, attachments )**
Use sendInteraction to send a "responded" statement to the configured LRS.

#### **Arguments**
response (Array): An array that holds the response(s) of an interaction. See the "Interaction Response Formats" table below to see the correct formatting.

interaction (Object): The interaction object defining the interaction activity.

parent ( Object ): The parent configuration object.

otherResponse ( string ): A provided extra response if an interaction has multiple responses. To be use with CHOICE_WITH_EXPLANATION
interaction type. The extra response will be added to the result.extensions property of the statement.

attachments ( Array ): An array of attachment objects. See ELearningXAPI.attachment() example shown in "Uploading a File" section below.
#### **Returns**
{}: Returns a Promise. On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code. 

Http error codes can be found here https://github.com/adlnet/xAPI-Spec/blob/master/xAPI-Communication.md#errorcodes

**Example**
```
// grab interaction constants
const { CHOICE, CHOICE_WITH_EXPLANATION } = eLearningLib.INTERACTION;

const choiceComponentList = [
  { id: "choice01-pensacola", description:"Pensacola" },
  { id: "choice02-norfolk", description:"Norfolk" },
  { id: "choice03-jacksonville", description:"Jacksonville" },
  { id: "choice04-sandiego", description:"San Diego" },
].map(item => eLearningLib.createInteractionComponentList(item.id, item.description));

const choiceWithExplanationComponentList = [
  { id: "choice01-yes", description:"answer1 example" },
  { id: "choice02-no", description:"answer2 example" },
].map(item => eLearningLib.createInteractionComponentList(item.id, item.description));

let choiceInteraction = eLearningLib.createInteraction(
  CHOICE,
  "https://navy.mil/netc/xapi/activities/cmi.interactions/09ee623e-72f4-471b-9b4c-94933bbb51e9",  
  "Question 2",
  "The Blue Angels’ home base has been located at which Naval Air Station since 1954.",
  ["choice01-pensacola"],
  choiceComponentList
);

let choiceWithExplanation = eLearningLib.createInteraction(
  CHOICE_WITH_EXPLANATION,
  "https://navy.mil/netc/xapi/activities/cmi.interactions/09ee623e-72f4-471b-9b4c-94933bbb51e0",
  "Question 5",
  "Is the Blue Angels’ home base located at Norfolk, VA since 1954. If no, then where is the base located.",
  ["choice02-no"],
  choiceWithExplanationComponentList
)

let interactionParent = eLearningLib.createParent(
  "https://navy.mil/netc/xapi/activities/assessments/c66edc28-aa6b-11ea-bb37-0242ac130002",
  "Naval Aviation Quiz",
  "This is an example assessment on Naval aviation history."
);

// send a choice interaction statement 
async function myApplication(){
  let choice = await eLearningLib.sendInteraction(
    ["choice01-pensacola"],
    choiceInteraction,        
    interactionParent
  ).catch( error => console.log(`An error has occurred ${error}`));

  let choiceWithExplanation = await eLearningLib.sendInteraction(
    ["choice02-no"],
    choiceWithExplanation,        
    interactionParent,
    "Pensacola" // using otherResponse param 
  ).catch( error => console.log(`An error has occurred ${error}`));

}

myApplication();
```
***
                                                                                                                       
### Interaction Constants
* Interaction constants can be used within an application to help identify what kind of interaction type the response statement will be sending.

| Interaction Types | Description                                                                                                                                                                                                                                                                                                                         |
| :---------------- | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| TRUE_FALSE        | An interaction with two possible responses: true or false.                                                                                                                                                                                                                                                                          |
| CHOICE            | An interaction with a number of possible choices from which the learner can select. This includes interactions in which the learner can select only one answer from the list and  those where the learner can select multiple items.                                                                                                |
| FILL_IN           | An interaction which requires the learner to supply a short response in the form of  one or more strings of characters. Typically, the correct response consists of part of a word, one word or a few words. "Short" means that the correct responses pattern and learner response strings will normally be 250 characters or less. |
| LONG_FILL_IN      | An interaction which requires the learner to supply a response in the form of a long string of characters.  "Long" means that the correct responses pattern and learner response strings will normally be more than 250 characters.                                                                                                 |
| MATCHING          | An interaction where the learner is asked to match items in one set (the source set) to items in another set (the target set). Items do not have to pair off exactly and it is possible for multiple or zero source items to be matched to a given target and vice versa.                                                           |
| PERFORMANCE       | An interaction that requires the learner to perform a task that requires multiple steps.                                                                                                                                                                                                                                            |
| SEQUENCING        | An interaction where the learner is asked to order items in a set.                                                                                                                                                                                                                                                                  |
| LIKERT            | An interaction which asks the learner to select from a discrete set of choices on a scale                                                                                                                                                                                                                                           |
| NUMERIC           | Any interaction which requires a numeric response from the learner.                                                                                                                                                                                                                                                                 |
| OTHER             | Another type of interaction that does not fit into those defined above.                                                                                                                                                                                                                                                             |
***

## **Uploading a File**
In some cases an Attachment is logically an important part of a Learning Record. It could be an essay, a video, etc. Another example of such an Attachment is (the image of) a certificate that was granted as a result of an experience. It is useful to have a way to store these Attachments in and retrieve them from an LRS.

### **ELearningXAPI.attachment( value, usageType, contentType, display, description )**

Use attachment to build an attachment object.

#### **Arguments**
value (string) :  An array buffer containing the data to post. 

usageType (string) : Identifies the usage of this attachment.

contentType (string) : The content type of the attachment.

display (string) Display name (title) of this attachment. 

description (string) A description of the attachment.

#### **Returns**
{}: Returns an attachment object to be used in sendInteraction(); 

**Example**

```
let arrayBuffer = null;
const reader = new FileReader();  
reader.onload = async function () {
  arrayBuffer = this.result;
};
// some file you upload through a input file field
reader.readAsArrayBuffer(inputFile.files[0]);
const attachment = eLearningLib.attachment(
  arrayBuffer,
  "image",
  "image/jpeg",
  "Image Attachment.",
  "Uploading An Image Attachment."
);

  // grab interaction constants
const { OTHER_UPLOAD_ATTACHMENT } = eLearningLib.INTERACTION;

let interaction = eLearningLib.createInteraction(
  OTHER_UPLOAD_ATTACHMENT,
  "https://navy.mil/netc/xapi/activities/cmi.interactions/dbcd346a-b828-4c20-9d84-3a817f6b6048",  
  "Question 11",
  "Please upload your PDF or Word doc essay"
);

let interactionParent = eLearningLib.createParent(
  "https://navy.mil/netc/xapi/activities/assessments/c66edc28-aa6b-11ea-bb37-0242ac130002",
  "Naval Aviation Quiz",
  "This is an example assessment on Naval aviation history."
);

// send a choice interaction statement 
  const response = await eLearningLib.sendInteraction(
    ["File uploaded: MyEssay.docx"],
    interaction,        
    interactionParent,
    null,
    [attachment]
  ).catch( error => console.log(`An error has occurred ${error}`));

```
***

## **Viewing a Page**
Viewing a page is an optional, general activity that could occur in nearly any type of content. The Viewed Page Statement is typically associated with the user arriving at, accessing, entering or visiting a page. The Viewed Page Statement does not imply that the user physically viewed the contents of the page. 

### **ELearningXAPI.viewed( id, name, description )**
Use viewed to send a "viewed" statement to the configured LRS.

#### **Arguments**
id (String) : A unique identifier for the interaction activity according to the Activity ID requirements in the NETC Common Reference Profile.

name (String): A value that represents  the official name or title of the Activity. 

description (String): A value that represents a short description of the Activity. 

#### **Returns**
{}: Returns a Promise. On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code. 

Http error codes can be found here https://github.com/adlnet/xAPI-Spec/blob/master/xAPI-Communication.md#errorcodes

**Example**

```
async function myApplication() {
  let result = await eLearningLib.viewed(
          "https://navy.mil/netc/xapi/activities/pages/a0db418e-961d-416d-9051-49ac3f812bc2",
          "Advanced Airborne Sensor Course informational page.",
          "An informational page used for the Advanced Airborne Sensor announcement."
      )
      .catch( error => console.log(`An error has occurred ${error}`));
}

myApplication();
```
***
## **Completing a Section**
Sections are activities that provide an optional way to organize parts of a lesson. If sections are used to organize the parts of a lesson, the responsibility falls on the content to generate an xAPI Statement that informs NETC’s LRS that the learner has completed a section within a lesson. The final completion of a section SHALL be communicated by using a Completed Statement. All of the NETC xAPI requirements for a Completed Section Statement are provided below. 

### **ELearningXAPI.section( id, name, description )**
Use viewed to send a "viewed" statement to the configured LRS.

#### **Arguments**
id (String) : A unique identifier for the interaction activity according to the Activity ID requirements in the NETC Common Reference Profile.

name (String): A value that represents  the official name or title of the Activity. 

description (String): A value that represents a short description of the Activity. 

#### **Returns**
{}: Returns a Promise. On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code. 

Http error codes can be found here https://github.com/adlnet/xAPI-Spec/blob/master/xAPI-Communication.md#errorcodes

**Example**

```
async function myApplication() {
  let result = await eLearningLib.section(
          "https://navy.mil/netc/xapi/activities/pages/a0db418e-961d-416d-9051-49ac3f812bc2",
          "Advanced Airborne Sensor Course informational page.",
          "An informational page used for the Advanced Airborne Sensor announcement."
      )
      .catch( error => console.log(`An error has occurred ${error}`));
}

myApplication();
```
***
## **Setting a Bookmark**
Learners can bookmark their location within an e-learning lesson with the intent on coming back to it at a later time.
Setting a bookmark allows the developer to store simple or complex state. For a course that has many lessons it's recommended
to use a more detailed state storing the starting, completion, and current location of the course. 
### **ELearningXAPI.setBookmark( location )**
Use setBookmark to set a location.

#### **Arguments**
location (String): A string describing a location.  

#### **Returns**
{}: Returns a Promise. On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code. 

Http error codes can be found here https://github.com/adlnet/xAPI-Spec/blob/master/xAPI-Communication.md#errorcodes

**Example**

```
// A trivial example to show a basic use of setBookmark
async function myApplication() {
  // set section 2 page 1 bookmark
  let result = await eLearningLib.setBookmark( "S02|P01" )
                .catch( error => console.log(`An error has occurred ${error}`));

                
// A more detailed bookmark example.
// This example uses statuses like started, completed, and lock to inform the application what state the course is currently in.
// These statuses could be helpful when setting UI states within an application ;)
// The currentLesson and currentPage properties lets the application know what lesson and what page the course was left off at.
const detailedBookmark = JSON.stringify(
  { 
    currentLesson: 1,
    currentPage: 0,
    course: { // course level
      started: true, 
      completed: false, 
      lock: true, 
      lessons: [ // lesson level
        { // lesson 1 state statuses   
          started: true, 
          completed: true, 
          lock: false, 
          pages: [ // pages within a lesson level
            { // lesson 1 page 1 state statuses 
              started: true, 
              completed: true, 
              lock: false, 
            }
          ]
        },
        { // lesson 2 state statuses   
          started: true, 
          completed: false, 
          lock: false, 
          pages: [ // pages within a lesson level
            { // lesson 2 page 1 state statuses  
              started: true, 
              completed: false, 
              lock: false, 
            },
            { // lesson 2 page 2 state statuses 
              started: false, 
              completed: false, 
              lock: true, 
            }
          ]
        }
      ]
    }
  }
)

await eLearningLib.setBookmark(detailedBookmark)
                .catch( error => console.log(`An error has occurred ${error}`));

}

myApplication();


```
## **Getting a Bookmark**
When a location on a learning activity is bookmarked, it should be able to be retrieved by the user.

### **ELearningXAPI.getBookmark()**
Use getBookmark to get the bookmark location.

#### **Returns**
string: Returns a location string.

**Example**

```
async function myApplication() {
  // returns "S02|P01" 
  let location = eLearningLib.getBookmark();
  if (location) {
    gotoLocation(location);
  } else {
    startFromTheBeginning();
  }
}

myApplication();
```
## **Get Activity State**
The ELearningXAPI library tracks some internal state so it knows how to handle suspending and resuming a course.
Although bookmarking should get you far when needing to save course location and statuses, some times knowing if a
course has been terminated might come in handle. For example, If you want to give the user an option to reattempt a course or continue reviewing the course even though the course has been terminated then getActivityState can help.
Calling getActivityState when the course starts returns an object with values you can check against. One of these
values is the bookmark value that is set when using setBookmark(). 

### **ELearningXAPI.getActivityState(courseActivityId)**
Use getActivityState to get the interval ELearningXAPI state.

#### **Arguments**
courseActivityId (String): A string which is the course id.
#### **Returns**
string: Returns a state object.


| Name        | Type          | Description |
| ------------| :-----------: | :----- |
| registration | string | The registration id associated with the current course attempt |
| initialized  | boolean | A boolean representing if that course has been initialized |
| terminated   | boolean | A boolean representing if that course has been terminated |
| lastSuspendedAttempt   | string | A string representing the last suspended lesson attempt |
| bookmark     | string | A string representing a location value that was set by setBookmark() |
| lessons     | object | An object of lessons that represent the statuses of each lesson attempt |

**Example**

```
/* what gets returned from getActivityState 
{
  "registration":"735f9297-a555-4777-bc22-6e75805fddc9"
  "initialized":true
  "terminated":false
  "lastSuspendedAttempt":""
  "bookmark":"s1|l2"
  "lessons":{
    "https://navy.mil/netc/xapi/activities/lessons/48edc638-9e3e-4773-a516-d4ae1a48a1e0": [
      {
        "registration": "3b5ab8e3-6621-40a2-a27d-a305510584fc",
        "initialized": true,
        "terminated": false,
        "suspended": true,
        "bookmark": "",
        "result": {}
      }
    ]
  }
}
*/

async function myApplication() {
  let state = eLearningLib.getActivityState('https://navy.mil/netc/xapi/activities/courses/d5c891ad-355e-4e44-a3c9-c69b632f59b2');
  if (state.terminated) {
    showOptionToReattemptOrJustReviewTheCourse() // long function name to give an idea what could be done here
  } 
}

myApplication();
```

## **BASIC USAGE**
Below is a basic usage of the ELearningXAPI library. The application below first prepares the library by setting a group, setting an activity, creating an interaction parent, adding extensions before sending any statements out to the LRS. It's good practice to call resume() first to see if the course or lesson has already been initialized before calling initialize on a course or lesson. The resume() call will see if a lesson has been suspended and if so, return successfully,  otherwise will error out letting the application know an the lesson hasn't been suspended. If resume error outs then initialize should be called on the lesson if the course has already been initialized. 

```

const myApplication = async () => {
  // Depending on your deployment of the course, configuration LMS parameters may be passed to the Library via "TinCan Launch"
  // else create a configuration file to pass to the Library.
  // In this example, the library assumes the host / LMS will be providing the configuration params
  // through url parameters, so no need for a configuration object. 
  // For local development purposes, pass in a configuration object described in the "Launching Configuration" section.
  // This example uses a lock step course. Only certain ELearningXAPI function will be used to show a basic use of the library.

  let lessonComplete = false;
  let currentActivity = 0

  const eLearningLib = new ELearningXAPI();

  const { COURSE, LESSON } = eLearningLib.ACTIVITY;

  const courseActivityID = "https://navy.mil/netc/xapi/activities/courses/CNATT-ab50b5bc-b99f-11eb-8529-0242ac130004";

  const activities = [
    {
      id: "https://navy.mil/netc/xapi/activities/lessons/9dd5f696-96dd-49fa-a450-bcc8e36ae57e",
      name: "Lesson 1 Attempts",
      description: "An interactive e-learning course on how a course implements xAPI Statements."
    },
    {
      id: "https://navy.mil/netc/xapi/activities/lessons/1677e692-5820-4423-b8fe-98da2ab78574",
      name: "Lesson 2 Attempts",
      description: "How an attempt on a lesson works and how data about the lesson is expressed in xAPI Statements."
    }
  ]
  // Set initial Lesson activity
  eLearningLib.setActivity(
    "https://navy.mil/netc/xapi/activities/lessons/9dd5f696-96dd-49fa-a450-bcc8e36ae57e",
    "Lesson 1 Attempts",
    "How an attempt on a lesson works and how data about the lesson is expressed in xAPI Statements."
  );
  Set Course activity
  eLearningLib.setGroup(
    "https://navy.mil/netc/xapi/activities/courses/d5c891ad-355e-4e44-a3c9-c69b632f59b2",
    "E-Learning Content Example",
    "An interactive e-learning course on how a course implements xAPI Statements."
  );

  // to be used with interaction responses
  interactionParent = eLearningLib.createParent(
    "https://navy.mil/netc/xapi/activities/assessments/c66edc28-aa6b-11ea-bb37-0242ac130002",
    "Naval Aviation Quiz",
    "This is an example assessment on Naval aviation history."
  );

  const extensions = {
    "https://w3id.org/xapi/netc/extensions/school-center":
      "Center for Naval Aviation Technical Training (CNATT)",
    "https://w3id.org/xapi/navy/extensions/launch-location": "Ashore",
    "https://w3id.org/xapi/netc/extensions/user-agent": navigator.userAgent,
  };

  // add extensions that will be used for all statements
  eLearningLib.addExtensions(extensions);

  // call launch to set lrs credentials 
  await eLearningLib.launch()
  .then(result => console.log(`Launch successful ${result}`))
  .catch(error => console.warn(`An error has occurred when trying to launch ${error}`))

  // function to be called on application start
  const onCourseStart = async () => {
    // Call resume first before initializing to see if an attempt has been suspended for the current lesson
    // Lets try to resume the current lesson 
    await eLearningLib.resume()
    .then(goToPage) // it Looks like we can resume! Hopefully a bookmark was set to let the application know where it left off.
    .catch(resumeErrorHandler)
  }

  const goToPage = () => {
    // returns '1|1'

    const bookmark = eLearningLib.getBookmark();


    if (bookmark) {
      // update course state manager to the bookmarked page.
      dispatch({type: PAGE, payload: bookmark});
      or
      currentActivity = activities[bookmark.split('|')[1]]
      eLearningLib.setActivity(currentActivity.id, currentActivity.name, currentActivity.description)
    }
  }

  const resumeErrorHandler = async (error) => {
    // It's the developer responsibility to suspend a lesson and set a bookmark.
    // Since the application could not resume, which most likely means it was never suspended,
    // Let's initialize the course and then the initial lesson 
    await eLearningLib.initialize(COURSE)
        .then(result => {
        })
        .catch(error => console.log(`Oh no, an error has occurred ${error}`)); 
    }

    // Finally, lets initialize the current lesson.
    await eLearningLib.initialize(LESSON)
    .then(result => console.log('initialized lesson!'));
  }

  const terminateCourse = async () => {
    await eLearningLib.terminate(COURSE)
    .then(() => {
      console.log('Course successfully terminated')
    })
    .catch(error => {
      // some error has occurred so handle it
      console.log(error)
    });
  }

  // When getting to the end of a lesson then call terminate on the lesson
  // possible code within a navigation button event
  if (lessonComplete) {
    await eLearningLib.terminate(LESSON)
    .then(() => console.log('successfully terminated the lesson!'))
    .catch(error => console.log(`Oh no, an error has occurred ${error}`))
    .finally(() => cleanUp())
  } else {
    // set bookmark to section 2 page 10. This is a trivial example for the purpose of this example. 
    // Setting a bookmark can be way more detailed then this and is recommend to use this a course / lesson state manager.   
    await eLearningLib.setBookmark('1|1')
    .then(result => console.log('bookmark successfully saved!'))
  }
  
  // call onCourseStart to start the course.
  onCourseStart();  

  // call suspend when a user exits a course
  window.addEventListener("beforeunload", (event) => {
      await ELearningXAPI.suspend()
      .then(result => console.log('lesson suspended!'))
  })
}

// some event listener that fires on page load mounted
window.addEventListener('load', (event) => {
  myApplication();
});

```